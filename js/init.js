$(document).ready(function (){

  $('.catalog-toolbar select, .product-container select').selectOrDie();
  
  $('.sidebar input').iCheck({
      checkboxClass: 'icheckbox_minimal-grey',
      radioClass: 'iradio_minimal-grey'
  });
  $('table.product-cart input').spinner();
});